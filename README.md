# Infrastructure

Репозиторий представляющий нашу инфраструктуру в виде кода (IOC)

## Запуск конфигурации серверов на прод

```shell
ansible-playbook -i hosts-prod.yaml --ask-vault-pass site.yaml
```

### Деплой только nginx с LetsEncrypts

```shell
ansible-playbook -i hosts-prod.yaml nginx-letsencrypt.yaml
```

## Шифрование секретов

### Шифрование

```shell
ansible-vault encrypt roles/wireguard/vars/secrets.yaml
```

### Дешифрование

```shell
ansible-vault decrypt roles/wireguard/vars/secrets.yaml
```

## Ansible galaxy

### freeze dependency

```shell
ansible-galaxy list > requirements.yml
```

### install dependency

```shell
ansible-galaxy install -r requirements.yml
```